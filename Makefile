# This file is a part of stringfx library.
# Copyright © 2017-2019 Видершпан Евгений Сергеевич
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


LB=link.exe
CC=cl.exe
CFLAGS=/O2 /GA /nologo /D_UNICODE
OBJ=string_pointer_array.obj string_builder.obj binary_search.obj pointer_list.obj create_index.obj

test_it.exe: string_builder.lib test_it.c
	$(CC) $(CFLAGS) test_it.c string_builder.lib

string_builder.lib: $(OBJ)
	$(LB) /LIB /nologo /OUT:string_builder.lib $**

string_builder.obj: string_builder.c stringfx.h
	$(CC) $(CFLAGS) /c string_builder.c

string_pointer_array.obj: string_pointer_array.c stringfx.h
	$(CC) $(CFLAGS) /c string_pointer_array.c

binary_search.obj: binary_search.c stringfx.h
	$(CC) $(CFLAGS) /c binary_search.c

pointer_list.obj: pointer_list.c stringfx.h
	$(CC) $(CFLAGS) /c pointer_list.c

create_index.obj: create_index.c stringfx.h
	$(CC) $(CFLAGS) /c create_index.c

clean:
	del  test_it.exe test_it.obj $(OBJ) string_builder.lib
