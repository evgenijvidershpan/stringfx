/* This file is a part of stringfx library.
 Copyright © 2017-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stringfx.h"


int string_pointer_array_create(LPSTRING_POINTER_ARRAY lpv)
{
    if (!lpv)
        return 0;
    lpv->poits_count = 0;
    return 1;
}

int string_pointer_array_destroy(LPSTRING_POINTER_ARRAY lpv)
{
    int index, i, current_block_size;

    if (!lpv)
        return 0;
    // adjust the pointer to the first block
    index = STRING_POINTER_ARRAY_FAST_POINTS;
    // set size of the current block
    current_block_size = STRING_POINTER_ARRAY_FIRST_BLOCK;
    // release all allocated blocks
    for (i=0; i<STRING_POINTER_ARRAY_MEM_POINTS; ++i)
    {
        if (index >= lpv->poits_count)
            break;
        if (lpv->mem_points[i] != 0)
        {
            STRINGFX_FREE(lpv->mem_points[i]);
        }
        // adjust the pointer to the next block
        index += current_block_size;
        // set size of the next block
        // blocks are allocated with the size x2 from the previous,
        // but no grater than POINTER_ARRAY_MAX_BLOCK
        current_block_size *= 2;
        if (current_block_size > STRING_POINTER_ARRAY_MAX_BLOCK)
            current_block_size = STRING_POINTER_ARRAY_MAX_BLOCK;
    }
    lpv->poits_count = 0;
    return 1;
}


int string_pointer_array_add(LPSTRING_POINTER_ARRAY lparr,
                             const LPSTRING_POINTER lpv)
{
    int index, current_block_size, i, new_block_size;
    LPSTRING_POINTER pt;

    if (!lpv || !lparr)
        return 0;

    index = lparr->poits_count;

    if (index < STRING_POINTER_ARRAY_FAST_POINTS)
    {
        LPSTRING_POINTER pt = &lparr->fast_points[index];
        pt->pointer = lpv->pointer;
        pt->length = lpv->length;
        pt->opcode = lpv->opcode;
        lparr->poits_count++;
        return 1;
    }
    // adjust the pointer to the first block
    index -= STRING_POINTER_ARRAY_FAST_POINTS;
    // set size of the current block
    current_block_size = STRING_POINTER_ARRAY_FIRST_BLOCK;

    for (i=0; i<STRING_POINTER_ARRAY_MEM_POINTS; ++i)
    {
        if (index < current_block_size)
        {
            // if index = 0 we on new block
            if (index == 0)
            {
                new_block_size = current_block_size * sizeof(STRING_POINTER);
                lparr->mem_points[i] = (LPSTRING_POINTER)
                                            STRINGFX_MALLOC(new_block_size);
                // memory availability check
                if (lparr->mem_points[i] == 0)
                    return 0;
            }
            pt = &(lparr->mem_points[i][index]);
            pt->pointer = lpv->pointer;
            pt->length = lpv->length;
            pt->opcode = lpv->opcode;
            lparr->poits_count++;
            return 1;
        }
        // adjust the pointer to the next block
        index -= current_block_size;
        // set size of the next block
        // blocks are allocated with the size x2 from the previous,
        // but no grater than POINTER_ARRAY_MAX_BLOCK
        current_block_size *= 2;
        if (current_block_size > STRING_POINTER_ARRAY_MAX_BLOCK)
            current_block_size = STRING_POINTER_ARRAY_MAX_BLOCK;
    }
    return 0;
}


LPSTRING_POINTER string_pointer_array_get(LPSTRING_POINTER_ARRAY lparr,
                                          int index)
{
    int current_block_size, i;

    if (!lparr || index < 0)
        return 0;
    
    if (index < lparr->poits_count)
    {
        if (index < STRING_POINTER_ARRAY_FAST_POINTS)
        {
            return &lparr->fast_points[index];
        }
        // adjust the index to the first block
        index -= STRING_POINTER_ARRAY_FAST_POINTS;
        // set size of the first block
        current_block_size = STRING_POINTER_ARRAY_FIRST_BLOCK;

        for (i=0; i<STRING_POINTER_ARRAY_MEM_POINTS; ++i)
        {
            if (index < current_block_size)
            {
                return &(lparr->mem_points[i][index]);
            }
            // adjust the index to the next block
            index -= current_block_size;
            // blocks are allocated with the size x2 from the previous,
            // but no grater than POINTER_ARRAY_MAX_BLOCK
            current_block_size *= 2;
            if (current_block_size > STRING_POINTER_ARRAY_MAX_BLOCK)
                current_block_size = STRING_POINTER_ARRAY_MAX_BLOCK;
        }
        // unreachable piece of code
        *((char*)0)=0;
    }
    return 0;
}
