/* This file is a part of stringfx library.
 Copyright © 2017-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef STRINGFX_H_INCLUDED
#define STRINGFX_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#ifndef STRINGFX_MALLOC
#include <stdlib.h>
#define STRINGFX_MALLOC     malloc
#define STRINGFX_FREE       free
#endif

#ifndef _TCHAR_DEFINED
#ifdef _UNICODE
typedef unsigned short TCHAR;
#else
typedef char TCHAR;
#endif
#endif

/// For easy insertion of strings like "string"
#ifdef _UNICODE
#define STRING_BUILDER_ADD_STATIC(lpsb, strm, opcode)\
    string_builder_append(lpsb, L##strm, (sizeof(L##strm)/sizeof(TCHAR))-1, opcode)
#else
#define STRING_BUILDER_ADD_STATIC(lpsb, strm, opcode)\
    string_builder_append(lpsb, strm, (sizeof(strm)/sizeof(TCHAR))-1, opcode)
#endif

#define STRING_BUILDER_OP_NONE              0 /// No effect
#define STRING_BUILDER_OP_CHAR              1 /// Add symbols around
#define STRING_BUILDER_OP_XML               2 /// Adds an open or closed XML tag
#define STRING_BUILDER_OP_ADD               3 /// Add symbols after
#define STRING_BUILDER_OP_PREFIX            4 /// Add symbols before
#define STRING_BUILDER_OP_ADD_SPACE         5 /// Adds multiple spaces after
#define STRING_BUILDER_OP_PREFIX_SPACE      6 /// Adds multiple spaces before
#define STRING_BUILDER_OP_ADD_BRACKETS      7 /// Adds brackkets around

#define STRING_BUILDER_MASK_DATA(X)         (((unsigned long)X & 0xFF) << 8)

#ifdef _UNICODE
#define STRING_BUILDER_GET_MASK_DATA(X)     (((unsigned long)X & 0xFFFF00) >> 8)
#else
#define STRING_BUILDER_GET_MASK_DATA(X)     (((unsigned long)X & 0xFF00) >> 8)
#endif

#define STRING_BUILDER_MAKE_CHAR(X)         (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_CHAR)
#define STRING_BUILDER_MAKE_ADD(X)          (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_ADD)
#define STRING_BUILDER_MAKE_PREFIX(X)       (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_PREFIX)
#define STRING_BUILDER_MAKE_PREFIX_SPACE(X) (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_PREFIX_SPACE)
#define STRING_BUILDER_MAKE_ADD_SPACE(X)    (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_ADD_SPACE)
#define STRING_BUILDER_MAKE_ADD_XML(X)      (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_XML)
#define STRING_BUILDER_MAKE_ADD_BRACKETS(X) (STRING_BUILDER_MASK_DATA(X) | STRING_BUILDER_OP_ADD_BRACKETS)

#define STRING_BUILDER_OP_QUOTED            STRING_BUILDER_MAKE_CHAR('"')
#define STRING_BUILDER_OP_QUOTES            STRING_BUILDER_MAKE_CHAR('\'')
#define STRING_BUILDER_OP_RBRACKETS         STRING_BUILDER_MAKE_ADD_BRACKETS('(')
#define STRING_BUILDER_OP_FBRACKETS         STRING_BUILDER_MAKE_ADD_BRACKETS('{')
#define STRING_BUILDER_OP_BRACKETS          STRING_BUILDER_MAKE_ADD_BRACKETS('[')

/// Count of static STRING_POINTER in STRING_POINTER_ARRAY
#define STRING_POINTER_ARRAY_FAST_POINTS    256
/// Count of dynamic STRING_POINTER array's in STRING_POINTER_ARRAY
#define STRING_POINTER_ARRAY_MEM_POINTS     128
/// Cont of STRING_POINTER in first dynamic array
#define STRING_POINTER_ARRAY_FIRST_BLOCK    (0xffff / sizeof(STRING_POINTER))
/// Limit the size of allocated dynamic array
#define STRING_POINTER_ARRAY_MAX_BLOCK      ((1024*1024*32) / sizeof(STRING_POINTER))

/// A pointer to a string with its size and an additional field
typedef struct tagSTRING_POINTER {
    const TCHAR* pointer;
    int length;
    unsigned long opcode;
} STRING_POINTER, *LPSTRING_POINTER;

/// Storage of such pointers
typedef struct tagSTRING_POINTER_ARRAY {
    int poits_count;
    STRING_POINTER fast_points[STRING_POINTER_ARRAY_FAST_POINTS];
    LPSTRING_POINTER mem_points[STRING_POINTER_ARRAY_MEM_POINTS];
} STRING_POINTER_ARRAY, *LPSTRING_POINTER_ARRAY;

/// Initializing the pointers storage (or use lparr->poits_count = 0)
int string_pointer_array_create(LPSTRING_POINTER_ARRAY lparr);

/// Destroys a storage of pointers (not needed if
/// lparr->poits_count <= STRING_POINTER_ARRAY_FAST_POINTS)
int string_pointer_array_destroy(LPSTRING_POINTER_ARRAY lparr);

/// Add new pointer into pointers storage
int string_pointer_array_add(LPSTRING_POINTER_ARRAY lparr,
                             const LPSTRING_POINTER lpv);

/// Get the pointer to STRING_POINTER from pointers storage by index
LPSTRING_POINTER string_pointer_array_get(LPSTRING_POINTER_ARRAY lparr,
                                          int index);

/// String builder storage
typedef struct tagSTRING_BUILDER {
    int length; /// the number of characters that are expected on the output
    STRING_POINTER_ARRAY pointer_array;
} STRING_BUILDER, *LPSTRING_BUILDER;

/// Initialize string builder
int string_builder_create(LPSTRING_BUILDER lpv);

/// Add a string with opcode
int string_builder_append(LPSTRING_BUILDER lpv,
                          const TCHAR* lpstring,
                          int length,
                          int opcode);

/// Combine lines with processing
int string_builder_build(LPSTRING_BUILDER lpv,
                         TCHAR* dest,
                         int length);

/// Destroy the string builder
int string_builder_destroy(LPSTRING_BUILDER lpv);


/// substring searching (any chars)
const TCHAR* string_search(const TCHAR* data_start,
                           const TCHAR* data_end,
                           const TCHAR* search_start,
                           const TCHAR* search_end);


/// Static POINTER array size in POINTER_ARRAY
#define POINTER_ARRAY_FAST_POINTS           256
/// Count of dynamic array's in POINTER_ARRAY
#define POINTER_ARRAY_MEM_POINTS            255
/// Cont of STRING_POINTER in first dynamic array
#define POINTER_ARRAY_FIRST_BLOCK           (0xffff / sizeof(void*))
/// Limit the size of allocated dynamic array
#define POINTER_ARRAY_MAX_BLOCK             ((1024*1024*32) / sizeof(void*))

typedef void* POINTER;
typedef POINTER* LPPOINTER;

typedef struct tagPOINTER_ARRAY {
    int poits_count;
    POINTER fast_points[POINTER_ARRAY_FAST_POINTS];
    POINTER* mem_points[POINTER_ARRAY_MEM_POINTS];
} POINTER_ARRAY, *LPPOINTER_ARRAY;

/// Initializing the pointers storage (or use lparr->poits_count = 0)
int pointer_array_create(LPPOINTER_ARRAY lparr);

/// Add new pointer into pointers storage
int pointer_array_add(LPPOINTER_ARRAY lparr, POINTER lpv);

/// Get the pointer to pointer from pointers storage by index
LPPOINTER pointer_array_get(LPPOINTER_ARRAY lparr,int index);

/// Destroys a storage of pointers (not needed if
/// lparr->poits_count <= POINTER_ARRAY_FAST_POINTS)
int pointer_array_destroy(LPPOINTER_ARRAY lparr);

/// Create index for substrings in data
int create_index(LPPOINTER_ARRAY lparr,
                 const TCHAR* data_start,
                 const TCHAR* data_end,
                 const TCHAR* search_start,
                 const TCHAR* search_end);

#ifdef __cplusplus
}
#endif
#endif // STRINGFX_H_INCLUDED
