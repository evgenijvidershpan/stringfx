/* This file is a part of stringfx library.
 Copyright © 2017-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stringfx.h"


const TCHAR* string_search(const TCHAR* data_start,
                           const TCHAR* data_end,
                           const TCHAR* search_start,
                           const TCHAR* search_end)
{
    int search_length;
    int data_length;
    int search_left;
    const TCHAR* search_pos;

    search_length = search_end - search_start;
    data_length = data_end - data_start;

    if (!data_start || !search_start)
        return 0;
    if (data_length < search_length)
        return 0;

    search_pos = search_start;
    search_left = search_start - search_end;
    // uncomment if you want to search in many small lines
    while (data_start < data_end)// - search_left)
    {
        if (*data_start++ == *search_pos++)
        {
            search_left = search_pos - search_end;
            if (search_left == 0)
                return data_start - search_length;
            continue;
        }
        search_pos = search_start;
    }
    return 0;
}

