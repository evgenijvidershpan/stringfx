/* This file is a part of stringfx library.
 Copyright © 2017-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>

#include "stringfx.h"

void xml_test()
{
    STRING_BUILDER sb;
    TCHAR* buffer;
    int buff_size;
    _tprintf(_T("test xml begin\n"));
    if (string_builder_create(&sb))
    {
        STRING_BUILDER_ADD_STATIC(&sb, "div", STRING_BUILDER_MAKE_ADD_XML(0));
        STRING_BUILDER_ADD_STATIC(&sb, "pre", STRING_BUILDER_MAKE_ADD_XML(0));
        STRING_BUILDER_ADD_STATIC(&sb, "code", STRING_BUILDER_MAKE_ADD_XML(0));
        STRING_BUILDER_ADD_STATIC(&sb, "", STRING_BUILDER_MAKE_PREFIX('\n'));
        STRING_BUILDER_ADD_STATIC(&sb, "1 Line", STRING_BUILDER_OP_FBRACKETS);
        STRING_BUILDER_ADD_STATIC(&sb, "", STRING_BUILDER_MAKE_PREFIX('\n'));
        STRING_BUILDER_ADD_STATIC(&sb, "2 Line", STRING_BUILDER_OP_RBRACKETS);
        STRING_BUILDER_ADD_STATIC(&sb, "", STRING_BUILDER_MAKE_PREFIX('\n'));
        STRING_BUILDER_ADD_STATIC(&sb, "3 Line", STRING_BUILDER_OP_BRACKETS);
        STRING_BUILDER_ADD_STATIC(&sb, "", STRING_BUILDER_MAKE_PREFIX('\n'));
        STRING_BUILDER_ADD_STATIC(&sb, "code", STRING_BUILDER_MAKE_ADD_XML(1));
        STRING_BUILDER_ADD_STATIC(&sb, "pre", STRING_BUILDER_MAKE_ADD_XML(1));
        STRING_BUILDER_ADD_STATIC(&sb, "div", STRING_BUILDER_MAKE_ADD_XML(1));
        
        buff_size = (sb.length + 1) * sizeof(TCHAR);
        buffer = (TCHAR*) malloc(buff_size);
        
        if (string_builder_build(&sb, buffer, sb.length + 1))
        {
            _tprintf(_T("%s\n"), buffer);
        }
        free(buffer);
        string_builder_destroy(&sb);
    }
    _tprintf(_T("test xml end\n"));
}


void pt_test()
{
    POINTER_ARRAY pta;
    int i;
    _tprintf(_T("test pt begin\n"));

    if (pointer_array_create(&pta))
    {
        for (i=0; i<1000000; ++i)
        {
            pointer_array_add(&pta, "test");
        }
        pointer_array_destroy(&pta);
    }
    _tprintf(_T("test pt end\n"));
}


void idx_test()
{
    POINTER_ARRAY pta;
    int i;
    LPPOINTER pt;

    TCHAR sd[] =
      _T("Lorem ipsum dolor sit amet consectetuer  semper porttitor cursus")
      _T("Vestibulum. Elit Nulla et amet. Leo wisi Donec Duis facilisi id")
      _T("Donec Nulla et interdum.  a condimentum Sed tincidunt. Consequat")
      _T("metus Curabitur dictumst In fames Ut semper risus urna. Vestibulum")
      _T("enim. Mauris amet vestibulum eros dolor  pretium sem ac. Nullam")
      _T("Vestibulum non accumsan fringilla et tellus  amet et. Vel at.")
      _T("Lobortis dolor amet cursus dignissim vitae pellentesque quis dui")
      _T("In. Integer Suspendisse vitae sed pellentesque adipiscing. Sed")
      _T("cursus Lorem consectetuer justo orci amet eget Ut. Pulvinar pede")
      _T("tincidunt interdum neque. faucibus at Pellentesque dolor Morbi ")
      _T("tincidunt Phasellus amet malesuada ipsum. interdum Maecenas.");
    
    TCHAR ss[] = _T("amet");
    
    _tprintf(_T("test idx begin:\n"));

    if (pointer_array_create(&pta))
    {

        create_index(&pta, sd, sd+(sizeof(sd)/sizeof(sd[0]))-1,
                               ss, ss+(sizeof(ss)/sizeof(ss[0]))-1);

        for (i=0; i<pta.poits_count; ++i)
        {
            pt = pointer_array_get(&pta, i);
            if (pt)
            {
                _tprintf(_T("i:%d\n"), (const TCHAR*)(*pt)-sd);
            }
        }
        pointer_array_destroy(&pta);
    }
    _tprintf(_T("test idx end:\n"));
}


int main()
{
    _tprintf(_T("char size: %d\n"), sizeof(TCHAR));
    xml_test();
    pt_test();
    idx_test();
    system("pause");
    return 0;
}
