/* This file is a part of stringfx library.
 Copyright © 2017-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stringfx.h"

int string_builder_create(LPSTRING_BUILDER lparr)
{
    if (lparr)
    {
        lparr->length = 0;
        return string_pointer_array_create(&lparr->pointer_array);
    }
    return 0;
}


int string_builder_destroy(LPSTRING_BUILDER lparr)
{
    if (lparr)
    {
        lparr->length = 0;
        return string_pointer_array_destroy(&lparr->pointer_array);
    }
    return 0;
}


TCHAR* internal_write_one(TCHAR* begin,
                          const TCHAR* end,
                          LPSTRING_POINTER pt)
{
    // since this is an internal function, the parameters are valid.
    // no check.
    unsigned long op_data;
    const TCHAR* src;
    TCHAR ch, bo, bc;
    int len, i;

    op_data = pt->opcode;
    src = pt->pointer;
    len = pt->length;

    --end; // reserve space for '\0'
    if (begin < end) // more than 1 character left?
    {
        switch (op_data & 0xff)
        {
            case 0: // no op
            {
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
            } return begin; // works even with an empty string in pt->str

            case STRING_BUILDER_OP_CHAR: // add symbols around
            {
                ch = STRING_BUILDER_GET_MASK_DATA(op_data);
                *begin++ = ch; // at least 1 char at the output
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
                if (begin >= end)
                    break;
                *begin++ = ch;
            } return begin;

            case STRING_BUILDER_OP_ADD_BRACKETS:
            {
                bo = STRING_BUILDER_GET_MASK_DATA(op_data);
                bc = '?';
                switch (bo)
                {
                    case '{': bc = '}'; break;
                    case '[': bc = ']'; break;
                    case '(': bc = ')'; break;
                }
                if (bc == '?')
                    break;
                *begin++ = bo; // at least 1 char at the output
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
                if (begin >= end)
                    break;
                *begin++ = bc;
            } return begin;

            case STRING_BUILDER_OP_ADD: //to add a symbol after
            {
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
                if (begin >= end)
                    break;
                *begin++ = STRING_BUILDER_GET_MASK_DATA(op_data);
            } return begin;

            case STRING_BUILDER_OP_XML: // add as XML tag
            {
                *begin++ = '<'; // at least 1 char at the output
                if (STRING_BUILDER_GET_MASK_DATA(op_data))
                {
                    if (begin >= end)
                        break;
                    *begin++ = '/';
                }
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
                if (begin >= end)
                    break;
                *begin++ = '>';
            } return begin;

            case STRING_BUILDER_OP_PREFIX:
            {
                // at least 1 char at the output
                *begin++ = STRING_BUILDER_GET_MASK_DATA(op_data);
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
            } return begin;

            case STRING_BUILDER_OP_ADD_SPACE:
            {
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
                ch = STRING_BUILDER_GET_MASK_DATA(op_data);
                for (i=0; i < ch; ++i)
                {
                    if (begin >= end)
                        break;
                    *begin++ = ' ';
                }
            } return begin;

            case STRING_BUILDER_OP_PREFIX_SPACE:
            {
                TCHAR cnt = STRING_BUILDER_GET_MASK_DATA(op_data);
                for (i=0; i < cnt; ++i)
                {
                    if (begin >= end)
                        break;
                    *begin++ = ' ';
                }
                for (i=0; i<len; ++i) // just copy
                {
                    if (begin >= end)
                        break;
                    *begin++ = *src++;
                }
            } return begin;
        }
    }
    // write '\0' at the end
    *begin = 0;
    return 0;
}


int string_builder_append(LPSTRING_BUILDER lpv,
                          const TCHAR* lpstring,
                          int length,
                          int opcode)
{
    STRING_POINTER pt;

    if (!lpv)
        return 0;

    if (lpstring)
    {
        if (length <= 0)
        {
            length = 0;
            while (lpstring[length])
                ++length;
        }
    }
    else
        length = 0;

    pt.pointer = lpstring;
    pt.length = length;
    pt.opcode = opcode;

    if (string_pointer_array_add(&lpv->pointer_array, &pt))
    {
        switch (opcode & 0xff)
        {
            case STRING_BUILDER_OP_CHAR:
                lpv->length += 2;
            break;

            case STRING_BUILDER_OP_ADD_BRACKETS:
                switch (STRING_BUILDER_GET_MASK_DATA(opcode))
                {
                    case '{':
                    case '[':
                    case '(':
                    lpv->length += 2;
                    break;
                }
            break;

            case STRING_BUILDER_OP_ADD:
            case STRING_BUILDER_OP_PREFIX:
                lpv->length += 1;
            break;

            case STRING_BUILDER_OP_XML:
                lpv->length += 2;
                if (STRING_BUILDER_GET_MASK_DATA(opcode))
                    lpv->length += 1;
            break;

            case STRING_BUILDER_OP_ADD_SPACE:
            case STRING_BUILDER_OP_PREFIX_SPACE:
                lpv->length += STRING_BUILDER_GET_MASK_DATA(opcode);
            break;
        }
        lpv->length += length;
    }
    return 1;
}


int string_builder_build(LPSTRING_BUILDER lpv,
                         TCHAR* dest,
                         int length)
{
    TCHAR* begin;
    TCHAR* end;
    int i;
    int poits_count;

    if (!lpv || !dest || length < 1 || lpv->pointer_array.poits_count <= 0)
        return 0;

    begin = dest;
    end = begin + length;

    poits_count = lpv->pointer_array.poits_count;
    for (i = 0; i < poits_count && begin < end; ++i)
    {
        LPSTRING_POINTER pt = string_pointer_array_get(&lpv->pointer_array, i);
        if (!pt)
            return 0;
        begin = internal_write_one(begin, end, pt);
        if (!begin)
            return 0;
    }
    if (begin) // if all lines are processed
    {
        if (begin < end) // and the end was not reached
            *begin = 0;
        return begin - dest; // length of written string not including zero
    }
    return 0;
}


