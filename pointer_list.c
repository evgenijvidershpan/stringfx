/* This file is a part of stringfx library.
 Copyright © 2017-2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stringfx.h"


int pointer_array_create(LPPOINTER_ARRAY lpv)
{
    if (!lpv)
        return 0;
    lpv->poits_count = 0;
    return 1;
}

int pointer_array_destroy(LPPOINTER_ARRAY lpv)
{
    int current_block_size;
    int index;
    int i;

    if (!lpv)
        return 0;
    // adjust the pointer to the first block
    index = POINTER_ARRAY_FAST_POINTS;
    // set size of the current block
    current_block_size = POINTER_ARRAY_FIRST_BLOCK;
    // release all allocated blocks
    for (i=0; i<POINTER_ARRAY_MEM_POINTS; ++i)
    {
        if (index >= lpv->poits_count)
            break;

        if (lpv->mem_points[i] != 0)
        {
            STRINGFX_FREE(lpv->mem_points[i]);
        }

        // adjust the pointer to the next block
        index += current_block_size;
        // set size of the next block
        // blocks are allocated with the size x2 from the previous,
        // but no grater than POINTER_ARRAY_MAX_BLOCK
        current_block_size *= 2;
        if (current_block_size > POINTER_ARRAY_MAX_BLOCK)
            current_block_size = POINTER_ARRAY_MAX_BLOCK;
    }
    lpv->poits_count = 0;
    return 1;
}


int pointer_array_add(LPPOINTER_ARRAY lparr,
                      POINTER lpv)
{
    int index;
    int current_block_size;
    int i;
    int new_block_size;
    LPPOINTER pt;

    if (!lpv || !lparr)
        return 0;

    index = lparr->poits_count;

    if (index < POINTER_ARRAY_FAST_POINTS)
    {
        LPPOINTER pt = &(lparr->fast_points[index]);
        *pt = lpv;
        lparr->poits_count++;
        return 1;
    }
    // adjust the pointer to the first block
    index -= POINTER_ARRAY_FAST_POINTS;
    // set size of the current block
    current_block_size = POINTER_ARRAY_FIRST_BLOCK;

    for (i=0; i<POINTER_ARRAY_MEM_POINTS; ++i)
    {
        if (index < current_block_size)
        {
            // if index = 0 we on new block
            if (index == 0)
            {
                new_block_size = current_block_size * sizeof(POINTER);
                lparr->mem_points[i] = (LPPOINTER)
                                            STRINGFX_MALLOC(new_block_size);
                // memory availability check
                if (lparr->mem_points[i] == 0)
                    return 0;
            }
            pt = &(lparr->mem_points[i][index]);
            *pt = lpv;
            lparr->poits_count++;
            return 1;
        }
        // adjust the pointer to the next block
        index -= current_block_size;
        // set size of the next block
        // blocks are allocated with the size x2 from the previous,
        // but no grater than POINTER_ARRAY_MAX_BLOCK
        current_block_size *= 2;
        if (current_block_size > POINTER_ARRAY_MAX_BLOCK)
            current_block_size = POINTER_ARRAY_MAX_BLOCK;
    }
    return 0;
}


LPPOINTER pointer_array_get(LPPOINTER_ARRAY lparr,
                            int index)
{
    int current_block_size;
    int i;

    if (!lparr || index < 0)
        return 0;

    if (index < lparr->poits_count)
    {
        if (index < POINTER_ARRAY_FAST_POINTS)
        {
            return &(lparr->fast_points[index]);
        }
        // adjust the pointer to the first block
        index -= POINTER_ARRAY_FAST_POINTS;
        // set size of the current block
        current_block_size = POINTER_ARRAY_FIRST_BLOCK;

        for (i=0; i<POINTER_ARRAY_MEM_POINTS; ++i)
        {
            if (index < current_block_size)
            {
                return &(lparr->mem_points[i][index]);
            }
            index -= current_block_size;
            // blocks are allocated with the size x2 from the previous,
            // but no grater than POINTER_ARRAY_MAX_BLOCK
            current_block_size *= 2;
            if (current_block_size > POINTER_ARRAY_MAX_BLOCK)
                current_block_size = POINTER_ARRAY_MAX_BLOCK;
        }
        // unreachable piece of code
        *((char*)0)=0;
    }
    return 0;
}



